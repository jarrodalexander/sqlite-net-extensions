using Cirrious.CrossCore.Plugins;

namespace Xamarin.iOSUnitTestsProject1.Bootstrap
{
    public class SqlitePluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Sqlite.PluginLoader, Cirrious.MvvmCross.Plugins.Sqlite.Touch.Plugin>
    {
    }
}